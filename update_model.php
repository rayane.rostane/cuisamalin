<?php
session_start();
include 'bdd.php'; // Connexion à la base de données

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $projetCuisineID = $_GET['id'];
    $nomProjet = $_POST['nomProjet'] ?? '';
    $totalPrice = $_POST['totalPrice'] ?? 0;
    $meubles = $_POST['meubles'] ?? '[]';

    // Ajouter des messages de débogage
    error_log('Données reçues : ' . print_r($_POST, true));
    error_log('Projet Cuisine ID : ' . $projetCuisineID);

    try {
        $meubles = json_decode($meubles, true, 512, JSON_THROW_ON_ERROR);
        error_log('Meubles décodés : ' . print_r($meubles, true));

        $stmt = $pdo->prepare("UPDATE projetcuisine SET NomProjet = ?, totalPrice = ? WHERE ID = ?");
        $stmt->execute([$nomProjet, $totalPrice, $projetCuisineID]);

        $stmt = $pdo->prepare("DELETE FROM espacecuisine WHERE ProjetCuisineID = ?");
        $stmt->execute([$projetCuisineID]);

        foreach ($meubles as $meuble) {
            $meubleID = $meuble['meubleID'];
            $positionX = $meuble['positionX'];
            $positionY = $meuble['positionY'];
            $largeur = $meuble['largeur'];
            $longueur = $meuble['longueur'];

            $stmt = $pdo->prepare("INSERT INTO espacecuisine (ProjetCuisineID, MeubleID, PositionX, PositionY, Largeur, Longueur) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->execute([$projetCuisineID, $meubleID, $positionX, $positionY, $largeur, $longueur]);
        }

        $response = ['status' => 'success', 'projetCuisineID' => $projetCuisineID];
        error_log('Réponse : ' . json_encode($response));
        echo json_encode($response);
        exit;
    } catch (JsonException $je) {
        echo json_encode(['status' => 'error', 'message' => $je->getMessage()]);
        exit;
    } catch (PDOException $e) {
        echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
        exit;
    }
}
