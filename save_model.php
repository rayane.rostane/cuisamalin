<?php
session_start();
include 'bdd.php'; // Connexion à la base de données

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $userID = $_SESSION['userID'] ?? null;
    $nomProjet = $_POST['nomProjet'] ?? '';
    $meubles = $_POST['meubles'] ?? '[]';
    $totalPrice = $_POST['totalPrice'] ?? 0;

    if (!$userID) {
        echo json_encode(['status' => 'error', 'message' => 'Utilisateur non connecté']);
        exit;
    }

    if (empty($nomProjet)) {
        echo json_encode(['status' => 'error', 'message' => 'Nom du projet manquant']);
        exit;
    }

    // Ajouter des messages de débogage
    error_log('Données reçues : ' . print_r($_POST, true));
    error_log('Utilisateur ID : ' . $userID);

    try {
        $meubles = json_decode($meubles, true, 512, JSON_THROW_ON_ERROR);
        error_log('Meubles décodés : ' . print_r($meubles, true));

        $stmt = $pdo->prepare("INSERT INTO projetcuisine (UtilisateurID, NomProjet, totalPrice) VALUES (?, ?, ?)");
        $stmt->execute([$userID, $nomProjet, $totalPrice]);

        $projetCuisineID = $pdo->lastInsertId();
        error_log('Projet Cuisine ID : ' . $projetCuisineID);

        foreach ($meubles as $meuble) {
            $meubleID = $meuble['meubleID'];
            $positionX = $meuble['positionX'];
            $positionY = $meuble['positionY'];
            $largeur = $meuble['largeur'];
            $longueur = $meuble['longueur'];

            $stmt = $pdo->prepare("INSERT INTO espacecuisine (ProjetCuisineID, MeubleID, PositionX, PositionY, Largeur, Longueur) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->execute([$projetCuisineID, $meubleID, $positionX, $positionY, $largeur, $longueur]);
        }

        $response = ['status' => 'success', 'projetCuisineID' => $projetCuisineID];
        error_log('Réponse : ' . json_encode($response));
        echo json_encode($response);
        exit;
    } catch (JsonException $je) {
        echo json_encode(['status' => 'error', 'message' => $je->getMessage()]);
        exit;
    } catch (PDOException $e) {
        echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
        exit;
    }
}
