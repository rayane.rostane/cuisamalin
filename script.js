document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('addMeubleBtn').addEventListener('click', () => {
        const meubleOptions = document.getElementById('meubleOptions');
        meubleOptions.classList.toggle('hidden');
    });

    document.querySelectorAll('.room-selection button').forEach(button => {
        button.addEventListener('click', () => {
            const roomType = button.dataset.roomType;
            updateRoomType(roomType);
        });
    });

    const container = document.getElementById('container');
    const handle = container.querySelector('.resize-handle');

    handle.addEventListener('mousedown', startResize);

    fetch('index.php')
        .then(response => response.json())
        .then(data => {
            if (data.error) {
                console.error('Erreur:', data.error);
                return;
            }
            console.log('Données reçues:', data);
            const meubleOptions = document.getElementById('meubleOptions');
            data.forEach(meuble => {
                console.log('Ajouter meuble option:', meuble);
                const item = document.createElement('div');
                item.className = 'option-item';
                item.dataset.type = meuble.Nom;
                item.dataset.price = meuble.Prix;
                item.innerText = meuble.Nom.charAt(0).toUpperCase() + meuble.Nom.slice(1);
                item.addEventListener('click', () => {
                    addMeubleToToolbar(meuble.Nom, meuble);
                    updateTotalPrice(meuble.Prix);
                    meubleOptions.classList.add('hidden');
                });
                meubleOptions.appendChild(item);
            });
        })
        .catch(error => console.error('Erreur lors du chargement des meubles:', error));
});

function updateRoomType(roomType) {
    const container = document.getElementById('container');
    container.style.backgroundImage = '';
    container.classList.remove('carre', 'l', 't');
    container.classList.add(roomType);

    switch (roomType) {
        case 'carre':
            container.style.backgroundImage = "url('img/piece_carre.jpg')";
            document.querySelector('.resize-handle').style.display = 'block';
            break;
        case 'l':
            container.style.backgroundImage = "url('img/piece_en_l.jpg')";
            document.querySelector('.resize-handle').style.display = 'block';
            break;
        case 't':
            container.style.backgroundImage = "url('img/piece_en_t.jpg')";
            document.querySelector('.resize-handle').style.display = 'block';
            break;
    }
}

function startResize(event) {
    event.preventDefault();
    const container = document.getElementById('container');
    const dimensionInfo = document.getElementById('dimensionInfo');
    const initialWidth = container.offsetWidth;
    const initialHeight = container.offsetHeight;
    const initialMouseX = event.clientX;
    const initialMouseY = event.clientY;

    function onMouseMove(event) {
        const deltaX = event.clientX - initialMouseX;
        const deltaY = event.clientY - initialMouseY;
        const maxDelta = Math.max(deltaX, deltaY);

        const newSize = initialWidth + maxDelta;

        container.style.width = `${newSize}px`;
        container.style.height = `${newSize}px`;
        container.style.backgroundSize = "contain";

        const widthInMeters = (newSize / 100).toFixed(2); // assuming 100px = 1m
        const heightInMeters = (newSize / 100).toFixed(2);
        dimensionInfo.innerText = `Largeur: ${widthInMeters}m, Hauteur: ${heightInMeters}m`;
    }

    function onMouseUp() {
        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('mouseup', onMouseUp);

        const widthInMeters = (container.offsetWidth / 100).toFixed(2);
        const heightInMeters = (container.offsetHeight / 100).toFixed(2);
        const areaInSquareMeters = (widthInMeters * heightInMeters).toFixed(2);

        dimensionInfo.innerText = `Surface: ${areaInSquareMeters}m²`;
    }

    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);
}

function addMeubleToToolbar(type, meuble) {
    const toolbar = document.getElementById('toolbar');
    const item = document.createElement('div');
    item.className = 'toolbar-item';
    item.draggable = true;
    item.dataset.type = type;
    item.dataset.id = meuble.ID;
    item.dataset.price = meuble.Prix;

    const img = document.createElement('img');
    img.src = meuble.Image;
    img.className = 'meuble-image';

    const deleteBtn = document.createElement('button');
    deleteBtn.className = 'delete-btn';
    deleteBtn.innerText = 'X';
    deleteBtn.addEventListener('click', (event) => {
        event.stopPropagation();
        if (confirm(`Êtes-vous sûr de vouloir supprimer le meuble ${meuble.Nom} ?`)) {
            console.log(`Suppression du meuble avec ID: ${meuble.ID}`);
            removeMeuble(item.dataset.uniqueId);
            item.remove();
            updateTotalPrice(-meuble.Prix);
        }
    });

    item.appendChild(img);
    item.appendChild(deleteBtn);
    item.dataset.uniqueId = generateUUID();
    item.addEventListener('dragstart', (event) => {
        event.dataTransfer.setData('type', JSON.stringify(meuble));
        event.dataTransfer.setData('uniqueId', item.dataset.uniqueId);
    });

    toolbar.appendChild(item);
    console.log(`Meuble ajouté à la barre d'outils: `, meuble);
}

function removeMeuble(uniqueId) {
    console.log(`Suppression des meubles dans le plan avec unique ID: ${uniqueId}`);
    const container = document.getElementById('container');
    const meubles = container.querySelectorAll('.meuble');
    meubles.forEach(meuble => {
        console.log(`Vérification du meuble avec unique ID: ${meuble.dataset.uniqueId}`);
        if (meuble.dataset.uniqueId === uniqueId) {
            console.log(`Meuble trouvé et supprimé : ${uniqueId}`);
            meuble.remove();
        }
    });
}

function updateTotalPrice(amount) {
    const totalPriceElement = document.getElementById('totalPrice');
    let totalPrice = parseInt(totalPriceElement.innerText.replace(/[^0-9]/g, '')) || 0;
    totalPrice += amount;
    totalPriceElement.innerText = `Montant total: ${totalPrice}€`;
}

document.getElementById('container').addEventListener('dragover', (event) => {
    event.preventDefault();
});

document.getElementById('container').addEventListener('drop', (event) => {
    event.preventDefault();
    const meubleData = JSON.parse(event.dataTransfer.getData('type'));
    const uniqueId = event.dataTransfer.getData('uniqueId');
    console.log(`Ajout du meuble au plan avec unique ID: ${uniqueId}`);
    const container = document.getElementById('container');
    const meuble = document.createElement('div');
    meuble.className = 'meuble';
    meuble.dataset.uniqueId = uniqueId;
    meuble.dataset.id = meubleData.ID;
    meuble.style.left = `${event.clientX - container.getBoundingClientRect().left}px`;
    meuble.style.top = `${event.clientY - container.getBoundingClientRect().top}px`;
    meuble.style.width = `${meubleData.width}px`;
    meuble.style.height = `${meubleData.height}px`;
    meuble.style.backgroundImage = `url(${meubleData.plan})`;

    const rotateBtn = document.createElement('button');
    rotateBtn.className = 'rotate-btn';
    rotateBtn.innerText = '↻';
    rotateBtn.addEventListener('mousedown', startRotate);

    meuble.appendChild(rotateBtn);
    meuble.addEventListener('mousedown', startDrag);
    container.appendChild(meuble);
    console.log(`Meuble ajouté au plan: `, meuble);
});

function startDrag(event) {
    const selectedMeuble = event.target.closest('.meuble');
    const container = document.getElementById('container');
    const containerRect = container.getBoundingClientRect();
    const meubleRect = selectedMeuble.getBoundingClientRect();
    const offsetX = event.clientX - meubleRect.left;
    const offsetY = event.clientY - meubleRect.top;

    function onMouseMove(moveEvent) {
        let x = moveEvent.clientX - containerRect.left - offsetX;
        let y = moveEvent.clientY - containerRect.top - offsetY;

        // Vérifier les limites gauche et droite
        if (x < 0) {
            x = 0;
        } else if (x + meubleRect.width > containerRect.width) {
            x = containerRect.width - meubleRect.width;
        }

        // Vérifier les limites haut et bas
        if (y < 0) {
            y = 0;
        } else if (y + meubleRect.height > containerRect.height) {
            y = containerRect.height - meubleRect.height;
        }

        selectedMeuble.style.left = `${x}px`;
        selectedMeuble.style.top = `${y}px`;
    }

    function onMouseUp() {
        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('mouseup', onMouseUp);
    }

    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);
}

function startRotate(event) {
    const selectedMeuble = event.target.closest('.meuble');
    const initialMouseX = event.clientX;
    const initialMouseY = event.clientY;
    const initialRotation = getRotationDegrees(selectedMeuble);

    function onRotateMove(rotateEvent) {
        const dx = rotateEvent.clientX - initialMouseX;
        const dy = rotateEvent.clientY - initialMouseY;
        const angle = initialRotation + Math.atan2(dy, dx) * (180 / Math.PI);
        selectedMeuble.style.transform = `rotate(${angle}deg)`;
    }

    function onRotateUp() {
        document.removeEventListener('mousemove', onRotateMove);
        document.removeEventListener('mouseup', onRotateUp);
    }

    document.addEventListener('mousemove', onRotateMove);
    document.addEventListener('mouseup', onRotateUp);
}

function getRotationDegrees(element) {
    const st = window.getComputedStyle(element, null);
    const tr = st.getPropertyValue("transform");
    if (tr === "none" || !tr) {
        return 0;
    }
    const values = tr.split('(')[1].split(')')[0].split(',');
    const a = values[0];
    const b = values[1];
    const angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
    return angle < 0 ? angle + 360 : angle;
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}