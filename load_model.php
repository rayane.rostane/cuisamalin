<?php
session_start();
include 'bdd.php'; // Connexion à la base de données

$projetCuisineID = $_GET['id'];

try {
    $stmt = $pdo->prepare("SELECT * FROM projetcuisine WHERE ID = ?");
    $stmt->execute([$projetCuisineID]);
    $model = $stmt->fetch(PDO::FETCH_ASSOC);
    $totalPrice = $model['totalPrice'] ?? 0;

    $stmt = $pdo->prepare("SELECT * FROM espacecuisine WHERE ProjetCuisineID = ?");
    $stmt->execute([$projetCuisineID]);
    $meubles = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Ajouter le chemin du plan pour chaque meuble
    foreach ($meubles as &$meuble) {
        $meubleID = $meuble['MeubleID'];
        $stmt = $pdo->prepare("SELECT plan FROM meuble WHERE ID = ?");
        $stmt->execute([$meubleID]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $meuble['plan'] = $result['plan'];
    }

    header('Content-Type: application/json');
    $response = [
        'projetCuisineID' => $model['ID'],
        'NomProjet' => $model['NomProjet'],
        'totalPrice' => $totalPrice,
        'meubles' => $meubles
    ];
    error_log('Réponse JSON : ' . json_encode($response)); // Ajoutez cette ligne
    echo json_encode($response, JSON_THROW_ON_ERROR);
    exit;
} catch (PDOException $e) {
    echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
    exit;
} catch (JsonException $je) {
    echo json_encode(['status' => 'error', 'message' => $je->getMessage()]);
    exit;
}