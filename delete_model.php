<?php
session_start();
include 'bdd.php'; // Connexion à la base de données

$projetCuisineID = $_GET['id'];

// Supprimer les meubles associés au projet de cuisine
$stmt = $pdo->prepare("DELETE FROM espacecuisine WHERE ProjetCuisineID = ?");
$stmt->execute([$projetCuisineID]);

// Supprimer le projet de cuisine
$stmt = $pdo->prepare("DELETE FROM projetcuisine WHERE ID = ?");
$stmt->execute([$projetCuisineID]);

header('Content-Type: application/json');
echo json_encode(['status' => 'success']);
exit;