<?php
session_start();
$userID = isset($_SESSION['userID']) ? $_SESSION['userID'] : 'Non connecté';
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plan de Cuisine Simplifié</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap">
</head>
<body>
    <header>
        <h1>Cuisimalin</h1>
    </header>
    <div class="user-id">
        Utilisateur ID : <?php echo htmlspecialchars($userID); ?>
    </div>
    <div class="main-content">
        <aside class="toolbar" id="toolbar">
            <button id="addMeubleBtn">Ajouter un meuble ▼</button>
            <div id="meubleOptions" class="hidden">
                <!-- Les options de meubles seront ajoutées ici dynamiquement -->
            </div>
            <p id="totalPrice">Montant total: 0€</p>
        </aside>
        <section class="content-area">
            <div class="room-selection" id="roomSelection">
                <h3>Choisissez la forme de votre pièce :</h3>
                <button data-room-type="carre">Pièce carrée</button>
                <button data-room-type="l">Pièce en L</button>
                <button data-room-type="t">Pièce en T</button>
            </div>
            <div class="container-wrapper">
                <div class="container" id="container">
                    <!-- Les meubles seront déposés ici -->
                    <div class="resize-handle"></div>
                    <div class="dimension-info" id="dimensionInfo"></div>
                </div>
            </div>
        </section>
    </div>
    <div class="model-controls">
        <button id="saveModelBtn">Sauvegarder le modèle</button>
        <button id="createNewModelBtn">Créer un nouveau modèle</button>
        <button id="listModelsBtn">Lister les modèles</button>
    </div>
    <div id="modelList">
        <!-- La liste des modèles sera affichée ici -->
    </div>
    <button class="logout-button" onclick="logout()">Déconnexion</button>
    <script src="script.js"></script>
    <script src="model.js"></script>
</body>
</html>
