<?php
include 'bdd.php';
$sql = "CALL CreerUtilisateur(@nouvelID)";  // Assurez-vous que le nom de la procédure correspond.
$stmt = $pdo->prepare($sql);
$stmt->execute();
// Récupérer la valeur du paramètre OUT
$sql = "SELECT @nouvelID AS IDGenere;";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);;

if ($result) {
    // Afficher l'ID de l'utilisateur nouvellement créé
    echo "Votre ID d'utilisateur est : " . $result['IDGenere'];
} else {
    // Aucun résultat ou erreur dans la procédure stockée
    echo "Aucun ID n'a été généré. Veuillez réessayer.";
}
?>

<form action="login.php" method="post">
        <label for="userID">Entrez votre ID pour vous connecter :</label>
        <input type="text" id="userID" name="userID" required>
        <input type="submit" name="connecter" value="Se connecter">
        <link rel="stylesheet" href="main.css">
</form>